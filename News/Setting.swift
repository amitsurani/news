//
//  VideoList.swift
//  News
//
//  Created by Amit Surani on 06/01/18.
//  Copyright © 2018 squareInfosoft. All rights reserved.
//

import UIKit
import Firebase

class Setting: UIViewController,UITableViewDataSource,UITableViewDelegate {

   
    @IBOutlet weak var settingTable: UITableView!
    var catSetting:NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        // self.settingTable.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.settingTable.frame.size.wvarh, height: 25))
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return ksettingCategories.count
        } else if section == 1 {
            return ksettingLanguages.count
        } else if section == 2 {
            return ksettingPlayer.count
        } else {
            return ksettingApp.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "My Feeds"
        } else if section == 1 {
            return "Languages"
        } else if section == 2 {
            return "Player"
        } else {
            return "App"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "setting", for: indexPath) as! cellForSetting
        if indexPath.section == 0 {
            cell.settingTitle.text = ksettingCategories[indexPath.row].name
            if ksettingCategories[indexPath.row].isEnable {
                cell.rightMark.isHidden = false
            } else {
                cell.rightMark.isHidden = true
            }
        } else if indexPath.section == 1 {
                cell.settingTitle.text = ksettingLanguages[indexPath.row].name
                if ksettingLanguages[indexPath.row].isEnable {
                    cell.rightMark.isHidden = false
                } else {
                    cell.rightMark.isHidden = true
                }
        } else if indexPath.section == 2 {
            cell.settingTitle.text = ksettingPlayer[indexPath.row]
            if UserDefaults.standard.bool(forKey: kuserPlayKey) {
                 cell.rightMark.isHidden = false
            } else {
                cell.rightMark.isHidden = true
            }
        } else {
            cell.rightMark.isHidden = true
            cell.settingTitle.text = ksettingApp[indexPath.row]
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = settingTable.cellForRow(at: indexPath) as! cellForSetting
        if indexPath.section == 0 {
            if ksettingCategories[indexPath.row].isEnable {
                if kuserFeed.count > 4 {
                    if let i = kuserFeed.index(of: ksettingCategories[indexPath.row].name) {
                        kuserFeed.remove(at: i)
                        ksettingCategories[indexPath.row].isEnable = false
                        cell.rightMark.isHidden = true
                    }
                } else {
                    Toast.OnlyMsg(message: "Minimum 4 Category should be selected", viewis: self.view)
                }
                UserDefaults.standard.set(kuserFeed, forKey: kuserCatKey)
            } else {
                ksettingCategories[indexPath.row].isEnable = true
                cell.rightMark.isHidden = false
                kuserFeed.append(ksettingCategories[indexPath.row].name)
                UserDefaults.standard.set(kuserFeed, forKey: kuserCatKey)
            }
        } else if indexPath.section == 1 {
            if ksettingLanguages[indexPath.row].isEnable {
                if kuserLang.count > 1 {
                    if let i = kuserLang.index(of: ksettingLanguages[indexPath.row].name) {
                        kuserLang.remove(at: i)
                        ksettingLanguages[indexPath.row].isEnable = false
                        cell.rightMark.isHidden = true
                    }
                } else {
                    Toast.OnlyMsg(message: "Minimum 1 Language should be selected", viewis: self.view)
                }
                UserDefaults.standard.set(kuserLang, forKey: kuserLangKey)
            } else {
                ksettingLanguages[indexPath.row].isEnable = true
                cell.rightMark.isHidden = false
                kuserLang.append(ksettingLanguages[indexPath.row].name)
                UserDefaults.standard.set(kuserLang, forKey: kuserLangKey)
            }
        } else if indexPath.section == 2 {
            if UserDefaults.standard.bool(forKey: kuserPlayKey) {
                UserDefaults.standard.set(false, forKey: kuserPlayKey)
                cell.rightMark.isHidden = true
            } else {
                UserDefaults.standard.set(true, forKey: kuserPlayKey)
                cell.rightMark.isHidden = false
            }
        } else {
            cell.settingTitle.text = ksettingApp[indexPath.row]
        }
    }
    
    @IBAction func onClickBack(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
