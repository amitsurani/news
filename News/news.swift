//
//  videos.swift
//  News
//
//  Created by Amit Surani on 08/01/18.
//  Copyright © 2018 squareInfosoft. All rights reserved.
//

import Foundation
import UIKit

class News:NSObject {
    
    var url = String()
    var videoId = String()
    var name = String()
    var thumbnail = String()
    var publisher = String()
    var publishedDate = String()
    var details = String()
    var duration = String()
    var ctaegoryName = String()
    var language = String()
    
    init(news:NSDictionary) {
        let check = news.value(forKey: "duration") as? String ?? ""
        if check != "" {
        if getDurationCount(duration: check) != 0 {
            self.url = news.value(forKey: "youtubeUrl") as! String
            self.videoId = news.value(forKey: "videoId") as! String
            self.name = news.value(forKey: "title") as! String
            self.thumbnail = news.value(forKey: "thumbnailUrl") as! String
            self.publisher = news.value(forKey: "channelTitle") as! String
            self.publishedDate = news.value(forKey: "publishedDate") as! String
            self.details = news.value(forKey: "description") as? String ?? ""
            self.duration = news.value(forKey: "duration") as? String ?? ""
            self.ctaegoryName = news.value(forKey: "categoryName") as! String
            self.language = news.value(forKey: "language") as! String
        }
        }
    }
}
