//
//  category.swift
//  News
//
//  Created by Amit Surani on 08/01/18.
//  Copyright © 2018 squareInfosoft. All rights reserved.
//

import Foundation

class category:NSObject {
    
    var name = String()
    var isEnable = Bool()
    
    init(catname:String) {
        self.name = catname
        self.isEnable = false
    }
    
}

class language:NSObject {
    
    var name = String()
    var isEnable = Bool()
    
    init(langname:String) {
        self.name = langname
        self.isEnable = false
    }
    
}

