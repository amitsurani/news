//
//  ViewController.swift
//  News
//
//  Created by Amit Surani on 06/01/18.
//  Copyright © 2018 squareInfosoft. All rights reserved.
//

import UIKit
import Firebase
import WillowTreeScrollingTabController


var accessFirebase:DatabaseReference!

class Categories: UIViewController,ScrollingTabControllerDelegate {
   
    
    @IBOutlet weak var scrollContainer: UIView!
    @IBOutlet weak var dataErrorLabel: UILabel!
    
    let scrollTab = ScrollingTabController()
    var viewControllers: [UIViewController] = []
    var viewControllerCount = ksettingCategories.count + 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        scrollTab.tabSizing = .fixedSize(105)
        scrollTab.tabTheme = .init(font: .systemFont(ofSize: 10), defaultColor: kgrayColor, selectedColor: kprimaryColor)
        scrollTab.tabView.selectionIndicator.backgroundColor = kprimaryColor
        scrollTab.tabView.selectionIndicatorHeight = 1.5
        
        ksettingCategories.append(category(catname:"MY FEED"))
        accessFirebase.child("category").queryLimited(toLast: 1).observe(.childAdded, with: { noOfCategory in
            accessFirebase.child("category").observe(.value, with: { categoryDetails in
                if let getData = categoryDetails.value as? NSArray {
                    for items in getData {
                        let dic = items as! NSDictionary
                        ksettingCategories.append(category(catname:dic.value(forKey: "name") as! String))
                        if kFirstUser == false {
                            kuserFeed.append(dic.value(forKey: "name") as! String)
                        }

                    }
                    self.viewControllerCount += Int(noOfCategory.key)!
                    self.buildViewControllers()
                    self.setupScrollTab()
                }
            })
        })
        accessFirebase.child("newsLanguages").observe(.childAdded, with: { languageDetails in
            ksettingLanguages.append(language(langname: languageDetails.value as! String))
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //scrollTab.selectTab(atIndex: 0, animated: true)
    }
    
    func setupScrollTab() {
        scrollTab.delegate = self
        scrollTab.willMove(toParentViewController: self)
        addChildViewController(scrollTab)
        scrollTab.viewControllers = viewControllers
        scrollTab.view.translatesAutoresizingMaskIntoConstraints = false
        scrollContainer.addSubview(scrollTab.view)
        scrollContainer.layoutIfNeeded()
        
        let horizontal = NSLayoutConstraint.constraints(withVisualFormat: "|[view]|", options: [], metrics: nil, views: ["view": scrollTab.view])
        let vertical = NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: ["view": scrollTab.view])
        NSLayoutConstraint.activate(horizontal + vertical)
        
        scrollTab.didMove(toParentViewController: self)
        scrollTab.selectTab(atIndex: 0, animated: true)
        
//        let tap = UITapGestureRecognizer()
//        scrollTab.tabView.collectionView.cellForItem(at: [0,0])?.addGestureRecognizer(tap)
        
        
       // scrollTab.centerSelectTabs = true
       
    }
    
    func buildViewControllers() {
        viewControllers.removeAll()
        for i in 0...viewControllerCount {
            let viewController = VideoList()
            if ksettingCategories.count > i {
                viewController.tabBarItem.title = ksettingCategories[i].name.uppercased()
                viewControllers.append(viewController)
            }
        }
    }
    

    func scrollingTabController(_ tabController: ScrollingTabController, displayedViewControllerAtIndex index: Int) {
        let viewController = VideoList()
        self.dataErrorLabel.text = "News not available for \(ksettingCategories[index].name)"
        
        if index == 0 {
            knewsListFilter.removeAll()
            var tempArray = [News]()
            var newsList = knewsList.count - 1
            var myFeed = kuserFeed.count - 1
            while myFeed >= 0 {
                let name = kuserFeed[myFeed]
                newsList = knewsList.count - 1
                while newsList >= 0 {
                    if name == knewsList[newsList].ctaegoryName {
                        tempArray.insert(knewsList[newsList], at: 0)
                    }
                    newsList -= 1
                }
                myFeed -= 1
            }
            var newsLangList = tempArray.count - 1
            var myFeedLang = kuserLang.count - 1
            while myFeedLang >= 0 {
                let name = kuserLang[myFeedLang]
                newsLangList = tempArray.count - 1
                while newsLangList >= 0 {
                    if name == tempArray[newsLangList].language {
                        knewsListFilter.append(tempArray[newsLangList])
                    }
                    newsLangList -= 1
                }
                myFeedLang -= 1
            }
        } else {
            knewsListFilter = knewsList.filter { item in
                return item.ctaegoryName.lowercased().contains(ksettingCategories[index].name.lowercased())
            }
        }
        
        if knewsListFilter != [] {
            viewController.videoListTable.isHidden = false
            self.dataErrorLabel.isHidden = true
            viewController.videoListTable.reloadData()
        } else {
            
            viewController.videoListTable.reloadData()
            viewController.videoListTable.isHidden = true
            self.dataErrorLabel.isHidden = false
        }
    }
    
    @IBAction func onClickSetting(_ sender: UIButton) {
        var settingCatcount = ksettingCategories.count - 1
        var myFeed = kuserFeed.count - 1
        if kuserFeed.count != 0 {
            while myFeed >= 0 {
                let name = kuserFeed[myFeed]
                settingCatcount = ksettingCategories.count - 1
                while settingCatcount >= 0 {
                    if name == ksettingCategories[settingCatcount].name {
                        ksettingCategories[settingCatcount].isEnable = true
                    }
                    settingCatcount -= 1
                }
                myFeed -= 1
            }
        } else {
            while settingCatcount >= 0 {
                kuserFeed.append(ksettingCategories[settingCatcount].name)
                ksettingCategories[settingCatcount].isEnable = true
                settingCatcount -= 1
            }
        }
        
        
        var settingLangcount = ksettingLanguages.count - 1
        var myLang = kuserLang.count - 1
        if kuserLang.count != 0 {
            while myLang >= 0 {
                let name = kuserLang[myLang]
                settingLangcount = ksettingLanguages.count - 1
                while settingLangcount >= 0 {
                    if name == ksettingLanguages[settingLangcount].name {
                        ksettingLanguages[settingLangcount].isEnable = true
                    }
                    settingLangcount -= 1
                }
                myLang -= 1
            }
        } else {
            while settingLangcount >= 0 {
                kuserLang.append(ksettingLanguages[settingLangcount].name)
                ksettingLanguages[settingLangcount].isEnable = true
                settingLangcount -= 1
            }
        }
        

        let vc = storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! Setting
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

class VideoList: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let videoListTable = UITableView()

    override func viewDidLoad() {
       super.viewDidLoad()
        self.videoListTable.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.videoListTable.delegate = self
        self.videoListTable.dataSource = self
        self.view.addSubview(videoListTable)
        
        self.videoListTable.showsVerticalScrollIndicator = false
        self.videoListTable.showsHorizontalScrollIndicator = false
        self.videoListTable.register(UINib(nibName: "videoListCell", bundle: nil), forCellReuseIdentifier: "videoList")
        self.videoListTable.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.videoListTable.frame.size.width, height: 125))
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return knewsListFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "videoList", for: indexPath) as! cellForVideoLIsting
        cell.separatorInset.left = 0
        cell.separatorInset.right = 0
        let video = knewsListFilter[indexPath.row]
        cell.thumblain.sd_setImage(with: URL(string: "\(video.thumbnail)"), completed: nil)
        cell.name.text = video.name
        cell.publishDate.text = getLocalTimeZone(UTCtime: video.publishedDate)
        cell.publisher.text = video.publisher
        cell.duration.text = " \(seprateDuration(duration: video.duration)) "
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PlayerVC") as! Player
        controller.playList = knewsListFilter
        controller.playIndex = indexPath.row
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }

}
