//
//  Player.swift
//  News
//
//  Created by Amit Surani on 06/01/18.
//  Copyright © 2018 squareInfosoft. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class Player: UIViewController,YTPlayerViewDelegate {

    @IBOutlet weak var player: YTPlayerView!
    @IBOutlet weak var constraintHeightofPlayer: NSLayoutConstraint!
    @IBOutlet weak var videoDetails: UITextView!
    @IBOutlet weak var prevVideo: UIBarButtonItem!
    @IBOutlet weak var nextVideo: UIBarButtonItem!
    
    var playerControls = [String:Int]()
    var playIndex = 0
    var playList = [News]()
    var currentPlay:News?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        self.player.delegate = self
        self.constraintHeightofPlayer.constant = (self.view.frame.height / 2 ) - 50
        self.player.setNeedsLayout()
//        self.player.layer.borderWidth = 2
//        self.player.layer.borderColor = UIColor.clear.cgColor
//        self.player.layer.cornerRadius = cardRedius
//        self.player.layer.shadowRadius = 2
//        self.player.layer.shadowColor = UIColor.black.cgColor
//        self.player.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
//        self.player.layer.shadowOpacity = 2
        
        if playIndex == 0 {
            self.prevVideo.isEnabled = false
        }
        if playIndex == playList.count  - 1  {
            self.nextVideo.isEnabled = false
        }
        if UserDefaults.standard.bool(forKey: kuserPlayKey) {
            self.playerControls = ["controls": 2, "playsinline": 1, "autohide": 1, "showinfo": 1, "autoplay": 1, "modestbranding": 1]
        } else {
         self.playerControls = ["controls": 2, "playsinline": 1, "autohide": 1, "showinfo": 1, "autoplay": 0, "modestbranding": 1]
        }
        self.currentPlay = self.playList[playIndex]
        self.player.load(withVideoId: "II2BMr4LzcU", playerVars: playerControls)
        //self.player.load(withVideoId: "\(self.currentPlay?.videoId ?? "")", playerVars: playerControls)
        self.videoDetails.attributedText = self.setDetails()
        // Do any additional setup after loading the view.
    }
    
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        
        switch state {
        case .ended:
            if self.playList.count  - 1  > playIndex {
                self.playIndex += 1
                self.currentPlay = self.playList[playIndex]
                self.videoDetails.attributedText = self.setDetails()
                self.player.load(withVideoId: "\(self.currentPlay?.videoId ?? "")", playerVars: playerControls)
                self.player.playVideo()
            } else {
                self.playIndex = 0
                self.currentPlay = self.playList[playIndex]
               self.videoDetails.attributedText = self.setDetails()
                self.player.load(withVideoId: "\(self.currentPlay?.videoId ?? "")", playerVars: playerControls)
            }
        default:
            break
        }
    }
    
    @IBAction func onClickPrev(_ sender: UIBarButtonItem) {
        if self.playIndex > 0 {
            self.nextVideo.isEnabled = true
            self.playIndex -= 1
            self.currentPlay = self.playList[playIndex]
            self.videoDetails.attributedText = self.setDetails()
            self.player.load(withVideoId: "\(self.currentPlay?.videoId ?? "")", playerVars: playerControls)
        }
        if self.playIndex == 0 {
            self.prevVideo.isEnabled = false
        }
    }
    
    @IBAction func onClickNext(_ sender: UIBarButtonItem) {
        if self.playList.count - 1 > playIndex {
            self.prevVideo.isEnabled = true
            self.playIndex += 1
            self.currentPlay = self.playList[playIndex]
            self.videoDetails.attributedText = self.setDetails()
            self.player.load(withVideoId: "\(self.currentPlay?.videoId ?? "")", playerVars: playerControls)
            self.player.playVideo()
        }
        if playIndex == playList.count - 1 {
            self.nextVideo.isEnabled = false
        }
    }
    
    func setDetails() -> NSMutableAttributedString{
        let video = self.currentPlay
        let title = video?.name ?? ""
        let publisher = video?.publisher ?? ""
        let descriptions = video?.details ?? ""
        let duration = seprateDuration(duration: video?.duration ?? "")
        let language = video?.language ?? ""
        let category = video?.ctaegoryName ?? ""
        
        let titleText = NSMutableAttributedString(string: "\(title)\n", attributes: [NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize: 15),NSAttributedStringKey.foregroundColor:kblackColor])
        let publisherText = NSMutableAttributedString(string: "By \(publisher)\n\n", attributes:[NSAttributedStringKey.font:UIFont.systemFont(ofSize: 13),NSAttributedStringKey.foregroundColor:kgrayColor])
        let descriptionText = NSMutableAttributedString(string: "     \(descriptions)\n\n", attributes:[NSAttributedStringKey.font:UIFont.systemFont(ofSize: 13),NSAttributedStringKey.foregroundColor:kblackColor])
        let durationText = NSMutableAttributedString(string: "Duration: \(duration)\n", attributes:[NSAttributedStringKey.font:UIFont.systemFont(ofSize: 13),NSAttributedStringKey.foregroundColor:kgrayColor])
        let languageText = NSMutableAttributedString(string: "Language: \(language)\n", attributes:[NSAttributedStringKey.font:UIFont.systemFont(ofSize: 13),NSAttributedStringKey.foregroundColor:kgrayColor])
        let categoryText = NSMutableAttributedString(string: "Category: \(category)", attributes:[NSAttributedStringKey.font:UIFont.systemFont(ofSize: 13),NSAttributedStringKey.foregroundColor:kgrayColor])
        
        titleText.append(publisherText)
        titleText.append(descriptionText)
        titleText.append(durationText)
        titleText.append(languageText)
        titleText.append(categoryText)
        
        return titleText
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
