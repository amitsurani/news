//
//  Custom Cell.swift
//  News
//
//  Created by Amit Surani on 06/01/18.
//  Copyright © 2018 squareInfosoft. All rights reserved.
//

import Foundation
import UIKit


class cellForVideoLIsting:UITableViewCell {
    
    @IBOutlet weak var thumblain: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var publishDate: UILabel!
    @IBOutlet weak var duration: UILabel!
}

class cellForSetting:UITableViewCell {
    @IBOutlet weak var settingTitle: UILabel!
    @IBOutlet weak var rightMark: UIImageView!
    
    
}

func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}
