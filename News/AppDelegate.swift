//
//  AppDelegate.swift
//  News
//
//  Created by Amit Surani on 06/01/18.
//  Copyright © 2018 squareInfosoft. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
   

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        
        kFirstUser = UserDefaults.standard.bool(forKey: kisFirstUserKey)
        
        Database.database().isPersistenceEnabled = true
        accessFirebase = Database.database().reference()
        accessFirebase.keepSynced(true)
        
        if kFirstUser == false {
            //if Int(categoryDetails.key)! == 1 {
            accessFirebase.child("newsItem").queryLimited(toLast: 2000).observe(.value, with: { newsItem in
                if let getData = newsItem.value as? [String:Any] {
                    for item in getData {
                        let dic = item.value as! NSDictionary
                        knewsList.insert(News(news:dic), at: 0)
                    }
                    
                }
            })
            
            // }
        } else {
            //if Int(categoryDetails.key)! == 1 {
            accessFirebase.child("newsItem").queryLimited(toLast: 2000).observe(.childAdded, with: { newsItem in
                if let getData = newsItem.value as? [String:Any] {
                    let dic = getData as NSDictionary
                    knewsList.insert(News(news:dic), at: 0)
                }
            })
            // }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

