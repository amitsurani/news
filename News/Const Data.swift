//
//  Const Data.swift
//  News
//
//  Created by Amit Surani on 06/01/18.
//  Copyright © 2018 squareInfosoft. All rights reserved.
//

import Foundation
import UIKit


let Toast = toast()

let kprimaryColor = UIColor.red
let kgrayColor = UIColor(hex:"9E9E9E")
let kblackColor = UIColor.black

var kFirstUser = false
let kisFirstUserKey = "isFirstUser"

var knewsList = [News]()
var knewsListFilter = [News]()
var ksettingCategories = [category]()
var ksettingLanguages = [language]()
var ksettingPlayer = ["Auto Play Next Video"]
var ksettingApp = ["Invite Firend","Rate the App","Feedback"]

let kuserCatKey = "myfeed"
let kuserLangKey = "mylang"
let kuserPlayKey = "myplayer"
var kuserFeed = UserDefaults.standard.stringArray(forKey: kuserCatKey) ?? [String]()
var kuserLang = UserDefaults.standard.stringArray(forKey: kuserLangKey) ?? [String]()
