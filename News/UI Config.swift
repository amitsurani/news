//
//  UI Config.swift
//  News
//
//  Created by Amit Surani on 06/01/18.
//  Copyright © 2018 squareInfosoft. All rights reserved.
//

import Foundation
import UIKit

let cardRedius:CGFloat = 5
let cardShadow:CGFloat = 4

class videoThumbnailView:UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.cornerRadius = 5
        self.layer.shadowRadius = 5
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.5
        
    }
}

class videoDurationLable:UILabel {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 3
        self.clipsToBounds = true
    }
}

class videoThumbnailImage:UIImageView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
}

class cardView:UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.cornerRadius = cardRedius
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        self.layer.shadowOpacity = 2
    }
}

class cardImage:UIImageView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = cardRedius
        self.clipsToBounds = true
    }
}

class cardBlur:UIVisualEffectView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = cardRedius
        self.clipsToBounds = true
    }
}

class toast: UIViewController {
    
    func OnlyMsg(message : String,viewis: UIView) {
        
        let toastLabel = UILabel(frame: CGRect(x: viewis.frame.size.width/2 - 150, y: viewis.frame.size.height-100, width: 300, height: 50))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 7)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 17
        toastLabel.clipsToBounds  =  true
        UIApplication.shared.windows[UIApplication.shared.windows.count - 1].addSubview(toastLabel)
        UIView.animate(withDuration: 2, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

func changeStatusBarColor()  {
    
    UIApplication.shared.statusBarStyle = .lightContent
    UINavigationBar.appearance().clipsToBounds = true
    let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
    let ststusColor = UIColor(hex: "FF6B0A")
    statusBar.backgroundColor = ststusColor
    
}

func getLocalTimeZone(UTCtime:String) -> String {
    // create dateFormatter with UTC time format
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
    let date = dateFormatter.date(from: UTCtime)// create   date from string
    
    
    // change to a readable time format and change to local time zone
    dateFormatter.dateFormat = "EEE, MMM d, yyyy - h:mm a"
    dateFormatter.timeZone = NSTimeZone.local
    
    let previousDate = date!
    
    let diffrent = Date().timeIntervalSince(previousDate)
    let sec = Int(diffrent)
    let mini = sec/60
    let hour = mini/60
    let day = hour/24
    
    if day > 0 {
        return "\(day)d ago"
    }
    if hour > 0 {
        return "\(hour)h ago"
    }
    if mini > 0 {
        return "\(mini)m ago"
    }
        
    else {
        if sec > 0 {
            return "\(sec)s ago"
        } else {
            return "Just Now"
        }
    }
    
    
}

func getDurationCount(duration:String) -> Int {
    let arr = duration.components(separatedBy: ":")
    let count = Int(arr[0])! + Int(arr[1])! + Int(arr[2])!
    return count
}

func seprateDuration(duration:String) -> String {
    let arr = duration.components(separatedBy: ":")
    if arr[0] != "0" {
        return duration
    } else {
        return arr[1] + ":" + arr[2]
    }
}

extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

